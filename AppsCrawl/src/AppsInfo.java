

import java.io.IOException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.Map.Entry;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


import com.mongoClient.singleton.GetMongoClient;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;


import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.KeepEverythingExtractor;


public class AppsInfo
{

	//DB Collection Names
	
	static MongoClient mongoClient = GetMongoClient.getConnection();
	static DB db =mongoClient.getDB("AppsDB");//crawler
	static DBCollection appsColl = db.getCollection("AppsLinkNew");//AllLinks
	static DBCollection AllColl = db.getCollection("crawldata");//crawldata
	static DBCollection sampleColl = db.getCollection("sampleCrawldata");
	static DBCollection slowUrl = db.getCollection("slowUrl");
	static DBCollection otherlangColl = db.getCollection("otherLangCrawldata");
	
	/*static DBCollection geodata = db.getCollection("geodata");
	static DBCollection parallelDB = db.getCollection("parallelWords");
	static DBCollection parallelTitleDB = db.getCollection("parallelTitle");
	static MongoClient clientCounter = new MongoClient("178.32.49.5", 27017);
	static DB db1 =clientCounter.getDB("crawler");//crawler	
	static DBCollection counters = db1.getCollection("counters");*/

	GetData getDataObj = new GetData();
	static int id=0;


	//declaration part
	String AppsRating = "", AppsMainContent = "", AppsMetakey = "", AppsCategory = "", AppsDomain = "", AppsauthorityTitle ="";
	String website = null, AppsSize = "",AppsPrice = "";
	String AppsTitle = "", AppsCompany = "", AppsVersion = "", AppsDescription = "",AppsLocation = "",AppsCompatibility = "";
	String AppsEmail ="",tempDescription="";

	String date = "",tempAppsDescription="",tempsiteDescription="";
	String siteTitle = "", siteDomain = "", siteDescription = "",SiteauthorityTitle = "", siteLocation = "",siteMaincontent = "";
	String siteMetakey = "", siteMaincategory = "",image="";

	int siteDescriptionLength = 0, AppsDescriptionLength=0;
	long siteAlexarank;

	Document doc1 = null;
	DBObject dbObject,AppsGeoLocation;
	DBObject dbObjlink = null, dbobjSite = null;

	BasicDBObject searchQuery = null;
	BasicDBObject updateObject = null;
	BasicDBObject updateSiteObject = new BasicDBObject();

	DBObject siteQuery = null;
	DBCursor siteCursor = null;

	BasicDBObject insertObject = new BasicDBObject();
	BasicDBObject insertSiteObject = new BasicDBObject();

	SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

	void AppsLink(DBObject dbObject)
	{
		String SeedUrl = dbObject.get("link").toString();
		//SeedUrl="https://play.google.com/store/apps/details?id=com.itjogot.SunilGongo";
		
		Date dNow = new Date();

		date = ft.format(dNow);


		try
		{
			//hit App seed url from here
			Document doc = Jsoup.connect(SeedUrl)
					.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
					.referrer("http://www.google.com") 
					.header("Accept-Language", "en")
					.timeout(10000)
					.followRedirects(false)
					.get();
			
			searchQuery = new BasicDBObject();
			searchQuery.put("link", Pattern.compile(Pattern.quote(SeedUrl)));
			updateObject = new BasicDBObject();
			updateObject.append("$set", new BasicDBObject().append("status",false));
			appsColl.update(searchQuery, updateObject);

			AppsCompany = getDataObj.getCompany(doc);
			website = getDataObj.getWebsite(doc);



			if(website.contains("Error:"))//if jsoup timeout for wesite url 
			{

				insertObject.put("seedUrl",SeedUrl);
				insertObject.put("Exception", website.substring(6));
				slowUrl.insert(insertObject);
				//System.out.println("slow website.....");
				insertObject.clear();
				
			}
			else if(website.equals("link not found"))//if visit website link not found
			{
				
				updateObject.clear();
			}

			else if(!website.contains("plus.google")&&!website.contains("sites.google")&&!website.contains("/facebook")&&!website.contains("mail")&&!website.contains("/twitter")&&!website.contains("error"))//||AppsCompany.contains("Facebook")||AppsCompany.contains("Twitter"))//in case if visit website link is socail medial link							
			{

				siteDomain = getDataObj.getDomainName(website);
				

				Document siteDoc = Jsoup.connect(SeedUrl)
						.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
						.referrer("http://www.google.com") 
						.header("Accept-Language", "en")
						.timeout(10000)
						.get();//hit the website link

				AppsTitle = getDataObj.getTitle(doc);
				AppsTitle = AppsTitle.replaceAll("[^\\w\\s]","");
				//System.out.println("AppsTitle : "+AppsTitle);


				//AppsauthorityTitle = AppsTitle;

				//AppsDomain = getDataObj.getDomainName(website);
				AppsDomain = "play.google.com";//for all apps domain is same 
				//System.out.println("AppsDomain : "+AppsDomain);

				//System.out.println("website links are***"+website);
				AppsCategory = getDataObj.getCategory(doc);//app category which present on app page
				AppsMainContent = getDataObj.getMainContent(doc);//get description as main content which displayed on app page
				AppsDescription = AppsMainContent+" "+AppsCategory;
				AppsMetakey = AppsMainContent;
				AppsVersion = getDataObj.getVirsion(doc);
				AppsSize = getDataObj.getSize(doc);
				AppsRating = getDataObj.getRating(doc);

				AppsPrice = getDataObj.getPrice(doc);
				AppsCompatibility = getDataObj.getCompatibility(doc);
				AppsLocation = getDataObj.getLocation(doc);
				//System.out.println("AppsLocation : "+AppsLocation);
				AppsGeoLocation = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint'," + "'coordinates' : [ "
						+ "[ 0.00, 0.00 ]" + "]" + "}" + "}");

				AppsEmail = getDataObj.getEmail(doc);


				insertObject.put("seedUrl",SeedUrl);
				insertObject.put("domainLink",AppsDomain);


				insertObject.put("title",AppsTitle);
				insertObject.put("authorityTitle",AppsTitle);


				insertObject.put("companyName",AppsCompany);
				insertObject.put("mainContent",AppsMainContent);
				insertObject.put("version",AppsVersion);
				insertObject.put("compatibility",AppsCompatibility);

				insertObject.put("size",AppsSize);
				insertObject.put("rating",AppsRating);
				insertObject.put("link",SeedUrl);
				insertObject.put("companyUrl",website);

				insertObject.put("keyword",AppsMetakey);
				insertObject.put("subCategory","AppsData");
				insertObject.put("mainCategory",AppsCategory);
				insertObject.put("amount",AppsPrice);
				insertObject.put("systemDate",date);
				insertObject.put("alexaRanking","0");

				insertObject.put("location",AppsLocation);
				insertObject.put("geoip",AppsGeoLocation);
				insertObject.put("moduleName","Tools");
				insertObject.put("developers",AppsEmail);//store emailid as developer which present on app page
				insertObject.put("translationStatus","true");
				insertObject.put("authorityStatus","true");
				insertObject.put("boilerpipeStatus","true");
				insertObject.put("NERStatus","false");
				insertObject.put("imageStatus","true");
				insertObject.put("status","false");
				insertObject.put("expiredDate","");
				insertObject.put("expiredStatus","live");


				siteTitle=siteDoc.title();
				if(siteTitle.contains("Android Apps on Google Play"))
				{
					siteTitle=siteTitle.substring(0, siteTitle.indexOf("Android Apps on Google Play"));
				}
				siteTitle = siteTitle.replaceAll("[^\\w\\s]","");
				//System.out.println("websiteTitle : "+siteTitle);
				if(!siteTitle.matches(".*\\w*"))//if website title empty or in any other language.						
				{
					siteTitle = AppsCompany;
				}



				siteMaincategory = AppsCategory ;

				try
				{
					siteMaincontent=KeepEverythingExtractor.INSTANCE.getText(siteDoc.text());
				}
				catch(Exception e)
				{

					siteMaincontent = siteDoc.text();
				}
				siteMetakey=getDataObj.getMetakey(siteDoc);



				if(siteMetakey.isEmpty())
				{
					siteMetakey = siteMaincontent;
				}
				//System.out.println("metaKey : "+siteMetakey);

				try
				{
					Element siteEle =siteDoc.select("meta[name=description]").first();
					siteDescription = siteEle.attr("content");
				}
				catch(Exception e)
				{
					siteDescription=siteMaincontent;
				}

				if(siteDescription.isEmpty()||siteDescription.split(" ").length<2)//check condition for description must have min 2 words
				{
					siteDescription=siteMaincontent;
				}

				
//Remove alexa,ner,image code				
/*				siteAlexarank = Alexa.getAlexaRanking(website);
		
				try
				{
					HashMap hm = stanford.getDataForAllClasses(AppsLocation);

					siteLocation = "";
					Set set = hm.entrySet();
					Iterator iterator = set.iterator();

					while (iterator.hasNext()) 
					{
						Map.Entry<String, LinkedList<String>> me = (Entry<String, LinkedList<String>>) iterator.next();


						String key = me.getKey();
						//System.out.print(key + "= ");

						LinkedList<String> ll = me.getValue();
						for (String l : ll) 
						{
							if(key=="location")
							{
								String loc=l;
								siteLocation=siteLocation+loc+",";
							}
						}

					}
				}
				catch(Exception e){e.printStackTrace();}

				
				String siteGeoLocation[] = null;
				siteGeoLocation = GeoLocation.updateLocation(siteLocation);

				MasterClass masterClass = new MasterClass(website); //passing it to master Class to fetch all socail links and phone numbers

				String twitterLink=masterClass.getTwitterLink();
				String facebookLink=masterClass.getFacebookLink();
				String linkedinLink=masterClass.getLinkedinLink();
				String googleLink=masterClass.getGoogleLink();
				String instagramLink=masterClass.getInstagramLink();
				String phone=masterClass.getPhone(); 
				String emailId=masterClass.getEmailId();
				String pinterestLink=masterClass.getPinterestLink();

				
				image="";
			try
			{
				image=new mydownloader().downloader(website);
				//new S3upload(new File("img/"+image),image);
				insertSiteObject.put("image",image);
				insertSiteObject.put("imageStatus","true");
			}
			catch(Exception e)
			{
				System.err.println(e);
				image="no.jpg";
				insertSiteObject.put("image",image);
				insertSiteObject.put("imageStatus","false");
			}
*/
				insertSiteObject.put("seedUrl", SeedUrl);
				insertSiteObject.put("domainLink",siteDomain);
				insertSiteObject.put("link", website);


				insertSiteObject.put("title",siteTitle);
				insertSiteObject.put("authorityTitle",siteTitle);



				insertSiteObject.put("mainContent",siteMaincontent);
				insertSiteObject.put("keyword",siteMetakey);
				insertSiteObject.put("mainCategory",AppsCategory);
				insertSiteObject.put("subCategory","Apps");
				insertSiteObject.put("moduleName","Tools");
				insertSiteObject.put("systemDate",date);
				insertSiteObject.put("location",AppsLocation);
				insertSiteObject.put("geoLocation","");
				insertSiteObject.put("alexaRanking",0);

				insertSiteObject.put("twitter_link","");
				insertSiteObject.put("facebook_link","");
				insertSiteObject.put("linkedin_link","");
				insertSiteObject.put("googleLink","");
				insertSiteObject.put("instagram_link","");
				insertSiteObject.put("companyNumber","");
				insertSiteObject.put("companyEmail","");
				insertSiteObject.put("pinterest_link","");	
				insertSiteObject.put("translationStatus","true");
				insertSiteObject.put("authorityStatus","true");
				insertSiteObject.put("boilerpipeStatus","true");
				insertSiteObject.put("NERStatus","false");
				insertSiteObject.put("image","");
				insertSiteObject.put("imageStatus","false");
				insertSiteObject.put("status","false");
				insertSiteObject.put("expiredDate","");
				insertSiteObject.put("expiredStatus","live");
				insertSiteObject.put("geoip",AppsGeoLocation);
				
				//insertSiteObject.put("geoip",JSON.parse( siteGeoLocation[1]));

				//synchronization for multithreading 

				//System.out.println("thread id : "+Thread.currentThread().getId());
				AppsDescriptionLength = AppsDescription.length();
				tempAppsDescription="";
				tempAppsDescription = AppsDescription.replaceAll("[^\\w\\s]","");

				if(tempAppsDescription.length()<(AppsDescriptionLength/2))//if description in other language
				{
					insertObject.put("description",AppsDescription);
					try
					{
						otherlangColl.insert(insertObject);
						insertObject.clear();
					}
					catch(Exception e)
					{

						e.printStackTrace();
						insertObject.clear();

					}
				}

				else if(AppsTitle.matches(".*\\w.*")&&siteTitle.matches(".*\\w.*")&&!siteTitle.contains("error"))

				{
					insertObject.put("description",tempAppsDescription);

					try
					{
						//insertObject.put("_id",id);
						sampleColl.insert(insertObject);
						AllColl.insert(insertObject);
						System.out.println("app data inserted successfully");
						insertObject.clear();
					}
					catch(Exception e)
					{
						e.printStackTrace();
						//AppsInfo.decSequence("userid");
						insertObject.clear();
						//System.out.println("Duplicate App link found");
						//	System.out.println("..............\n");
					}
				}


				siteDescriptionLength = siteDescription.length();
				tempsiteDescription="";
				tempsiteDescription = siteDescription.replaceAll("[^\\w\\s]","");



				/*
					else*/ if(tempsiteDescription.length()<(siteDescriptionLength/2))//if description in other language
					{
						try
						{
							siteDescription = siteDescription + " "+ AppsCategory;
							insertSiteObject.put("description",siteDescription);
							otherlangColl.insert(insertSiteObject);
							insertSiteObject.clear();
						}
						catch(Exception e)
						{
							e.printStackTrace();
							insertSiteObject.clear();
						}
					}
					else if(siteTitle.matches(".*\\w.*")&&!siteTitle.contains("error")&&!website.contains("error"))
					{
						siteDescription = tempsiteDescription + " "+ AppsCategory;
						//System.out.println("Description : "+siteDescription);

						insertSiteObject.put("description",siteDescription);
						try
						{
							//insertSiteObject.put("_id",id);

							sampleColl.insert(insertSiteObject);
							AllColl.insert(insertSiteObject);
							System.out.println("site data inserted successfully");

							insertSiteObject.clear();

							/*insertSiteObject.put("_id",id);
							insertSiteObject.put("link",SeedUrl);
							insertSiteObject.put("systemDate",date);
							insertSiteObject.put("subCategory","Apps");
							insertSiteObject.put("moduleName","Tools");

							datacountColl.insert(insertSiteObject);
							insertSiteObject.clear();*/
						}
						catch(DuplicateKeyException e)
						{
							
							insertSiteObject.clear();

							siteQuery = new BasicDBObject("link", website);
							siteCursor =  sampleColl.find(siteQuery);
							dbobjSite = siteCursor.next();

							tempDescription = dbobjSite.get("description").toString();
							siteMaincategory = dbobjSite.get("mainCategory").toString();
							if(!siteMaincategory.contains(AppsCategory))
							{
								tempDescription = tempDescription+", "+AppsCategory;
								siteMaincategory = siteMaincategory+", "+AppsCategory;
								//if website allready present then update the description and mainCategory
								updateSiteObject.append("$set", new BasicDBObject().append("description", tempDescription).append("mainCategory", siteMaincategory));
								AllColl.update(siteQuery, updateSiteObject);
								sampleColl.update(siteQuery, updateSiteObject);
								System.out.println("site data updated successfully");
								updateSiteObject.clear();
							}

						}						
						catch(Exception E)
						{
							insertSiteObject.clear();
							E.printStackTrace();
						}
					}
				


			}



		}
		catch(UnknownHostException e){e.printStackTrace();}
		catch(Exception e)
		{
			
			e.printStackTrace();
			searchQuery = new BasicDBObject();
			searchQuery.put("link", Pattern.compile(Pattern.quote(SeedUrl)));
			updateObject = new BasicDBObject();
			updateObject.append("$set", new BasicDBObject().append("status",false));
			appsColl.update(searchQuery, updateObject);
		}
		finally 
		{
			System.out.println("****************************************************************");
			System.out.println("fetched link id : "+ dbObject.get("_id"));
			/*searchQuery = new BasicDBObject();
		searchQuery.put("link", Pattern.compile(Pattern.quote(SeedUrl)));
		updateObject = new BasicDBObject();
		updateObject.append("$set", new BasicDBObject().append("status",false));
		appsColl.update(searchQuery, updateObject);*/


		}

	}

	/*synchronized static int getNextSequence(String name) throws Exception 
	{

		BasicDBObject find = new BasicDBObject();
		find.put("_id", name);
		BasicDBObject update = new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq", 1));
		DBObject obj = counters.findAndModify(find, update);
		return (int) obj.get("seq");
	}
	 */
	/*synchronized static void decSequence(String name) throws Exception 
	{

		BasicDBObject find = new BasicDBObject();
		find.put("_id", name);
		BasicDBObject update = new BasicDBObject();
		update.put("$inc", new BasicDBObject("seq", -1));
		DBObject obj = counters.findAndModify(find, update);
		//return (int) obj.get("seq");
	}
	 */


}	
