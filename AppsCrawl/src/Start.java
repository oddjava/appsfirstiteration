
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;


import com.mongoClient.singleton.GetMongoClient;
import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;



public class Start 
{


	static int count = 0; 
	public static void main( String[] args ) throws Exception
	{  
		System.out.println("Enter ip");
		String ip = new Scanner(System.in).nextLine();
		GetMongoClient.createConnection(ip);
		Start x = new Start();
		x.access();

		System.out.println("\n\nDone with Apps Crawling... :) ");
		//appsObj.AppsLink();


	}



	public void access()
	{



		//String SeedUrl;
		System.out.println("enter start id ");
		Scanner in = new Scanner(System.in);
		long start = in.nextLong();

		System.out.println("enter end id ");
		long end = in.nextLong();

		DBObject query = new BasicDBObject();
		query.put("_id",new BasicDBObject("$gte",start).append("$lte",end));
		query.put("status", new BasicDBObject("$ne",false));
		//DBCursor cursor =  AppsInfo.appsColl.find(new BasicDBObject("_id",new BasicDBObject("$gte",start).append("$lte",end)).append("status", new BasicDBObject("$ne",false)));
		DBCursor cursor =  AppsInfo.appsColl.find(query);
		//DBCursor cursor =  AppsInfo.appsColl.find();
		final int MYTHREADS = 10;


		HashSet<DBObject> set =new LinkedHashSet<>();
		//HashSet<Integer> setId =new LinkedHashSet<>();
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		String link;
		int id=0;
		DBObject present;

		while(cursor.hasNext())
		{
			present =cursor.next();

			if(set.size()==1000)
			{
				callThreads(set);
				set.clear();
			}
			else
			{
				set.add(present);
			}


		}
		//System.out.println("set size : "+set.size());

		if(set.size()>0)
		{
			callThreads(set);
			set.clear();
		}




		/********************************************************************************************************/



	}

	/*
	 * Calling Threads....
	 */
	static void callThreads(HashSet<DBObject> set)
	{
		ExecutorService executor = Executors.newFixedThreadPool(10); // using executor for running multiple threads

		for(DBObject dbObject : set)
		{
			Runnable worker = new MyRunnable(dbObject); // sending url for further process
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {

		}
	}


	static class MyRunnable implements Runnable
	{

		DBObject object;


		public MyRunnable(DBObject object) 
		{
			this.object = object;
		}

		@Override
		public void run() 
		{ 

			new AppsInfo().AppsLink(object);
			count++;
			System.out.println("link fetching : "+count);


		}
	}

}
