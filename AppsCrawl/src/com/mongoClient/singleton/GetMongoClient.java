package com.mongoClient.singleton;

import java.util.Scanner;

import com.mongodb.DB;
import com.mongodb.MongoClient;

public class GetMongoClient {

		
		//private static final MongoClient instance = new MongoClient("178.32.49.5", 27017);
		private static  MongoClient instance ;
		
	    //private constructor to avoid client applications to instantiate
	    private GetMongoClient(){}

	    public static MongoClient getConnection(){
	        return instance;
	    }
	    
	    @Override
	    protected void finalize() throws Throwable {
	    	System.out.println("Clossing DB client connection...!");
	    	instance.close();   
	    super.finalize();
	    }

	    public static void createConnection(String ip)
	    {
	    	instance=new MongoClient(ip, 27017);
	    }
	    public static void main(String[] args) {
		   
	    	DB db;
	    	
	    	for(int i=0;i<20;i++)
	    		db=instance.getDB("crawler");
		}
}
