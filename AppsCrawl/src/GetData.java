

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

import de.l3s.boilerpipe.BoilerpipeProcessingException;
import de.l3s.boilerpipe.extractors.KeepEverythingExtractor;


public class GetData 
{
	String title = "", virsion = "", compatibility = "", size = "", mainContent = "", rating = "", domain = "", email = "", tempEmail="";
	String company = "", website = null, tempLink = null, site="",  metaKey = "", category = "", price = "", location = "";
	String getTitle(Document doc)
	{
		title = "";
		title = doc.title();

		if(title.contains("� Android Apps on Google Play"))
		{
			title = title.substring(0, title.indexOf("� Android Apps on Google Play"));
			
		}
		return title;
	}
/*
 * Will return company Name
 */
	String getCompany(Document doc)
	{
		company = "";
		try
		{

			/*Element companyLink = doc.select("span[itemprop=name]").first();*/
			company = doc.select("span[itemprop=name]").first().text();
			

		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		//System.out.println("Company : "+company);
		return company;

	}

	String getVirsion(Document doc)
	{
		virsion = "";
		try
		{
			/*Element virsionLink = doc.select("div[itemprop=softwareVersion]").first();*/
			virsion = doc.select("div[itemprop=softwareVersion]").first().text();
			

		}
		catch(Exception e)
		{
		//	e.printStackTrace();
		}
		//System.out.println("SoftwareVersion : "+virsion);
		return virsion;
	}

	
	
	String getCompatibility(Document doc)
	{
		compatibility = "";
		try
		{
			/*Element virsionLink = doc.select("div[itemprop=softwareVersion]").first();*/
			compatibility = doc.select("div[itemprop=operatingSystems]").first().text();
			

		}
		catch(Exception e)
		{
		//	e.printStackTrace();
		}
		//System.out.println("compatibility : "+compatibility);
		return compatibility;
	}
	
	

	/*String getAppDescription(Document doc)
	{
		String description = "";
		try
		{
			//Element descriptionLink = doc.select("div[itemprop=description]").first();
			description = doc.select("div[itemprop=description]").first().text();
			

		}
		catch(Exception e){e.printStackTrace();}
		if(description.isEmpty())
			description=doc.body().text();
		
		return description;
	}*/

	String getSize(Document doc)
	{
		size = "";

		try
		{
			//Element sizeLink = doc.select("div[itemprop=fileSize]").first();
			size = doc.select("div[itemprop=fileSize]").first().text();
			

		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		//System.out.println("Size : "+size);

		return size;
	}

	
	String getMainContent(Document doc)
	{
		mainContent = "";
		try
		{
			mainContent = doc.select("div[itemprop=description]").first().text();
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		//System.out.println("mainContent : "+ mainContent);
		return mainContent;
	}
	
	
	
	
	
	
	
	
	
	
	String getRating(Document doc)
	{
		rating = "";

		try
		{
			//	Element ratingLink = doc.getElementsByClass("score").first();
			doc.getElementsByClass("score").first().text();
			rating = doc.getElementsByClass("score").first().text();
			

		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}

		//System.out.println("Rating : "+rating);
		return rating;
	}

	String getWebsite(Document doc)
	{
		int flag = 0;
		website = null;
		tempLink = null;
		site="";
		try{

			Elements links = doc.select("a.dev-link");
		
			for(Element link : links)
			{
				website=link.attr("href");
				
				//System.out.println("Link : "+website);
				
				if(link.text().contains("Visit website"))
				{
					
					flag=1;
					try
					{
					site = Jsoup.connect(website)
							.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
							.referrer("http://www.google.com") 
							.header("Accept-Language", "en")
							.timeout(10000)
							.followRedirects(true)
							.execute().url().toExternalForm();
							
							/*Jsoup.connect(website).timeout(10000)
	                        .followRedirects(true) //to follow redirects
	                        .execute().url().toExternalForm();		*/	
					
					//System.out.println("site : "+site);
					}
					catch(Exception e)
					{
					
						site="Error:"+e.getClass().getName();
					}
					
				}


			}
		}catch(Exception E)
		{
			//E.printStackTrace();
		}
		if(flag==0)
		{
			//System.out.println("visit website link not found");
			site = "link not found";
		}
		//System.out.println("Link : "+website);	
		return site;
	}


	String getMetakey(Document doc1)
	{
		metaKey = "";
		try
		{

			metaKey = doc1.select("meta[name=keywords]").first().attr("content");
			


		}
		catch(Exception e)
		{
			//e.printStackTrace();
			metaKey = "";
		}
		
		
		
		return metaKey;
	}

	String getCategory(Document doc)
	{
		category = "";
		try
		{
			category=doc.select("span[itemprop=genre]").first().text();
			
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		//System.out.println("Category : "+category);
		return category;		
	}

	String getPrice(Document doc)
	{
		
		price = "";
		try
		{
			for (Element meta : doc.select("span")) 
			{
				if (meta.text().toLowerCase().contains("install")) 
				{
					price = "Free";
					
					break;
				}
			}	
			if(price.isEmpty())
			{
				Element tempPrice = doc.select("meta[itemprop=price]").first();
				price=tempPrice.attr("content");
				price=price.substring(1);
				//System.out.println("price : "+price);
			}
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		//System.out.println("price : "+price);
		
		return price;
	}
	
	String getLocation(Document doc)
	{
		//System.out.println("in get location");
		location = "";
		try
		{
		
		location = doc.select("div[class=content physical-address]").first().text();
		
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
	
		
		
		return location;
	}


	public String getDomainName(String url) throws URISyntaxException 
	{
		domain = "";
		
		try{
	    URI uri = new URI(url);
	   
	    domain = uri.getHost();
	  
		}
		catch(Exception e)
		{
			//e.printStackTrace();
		}
		if(domain!=null)			
			return /*domain.startsWith("www.") ? domain.substring(4) :*/ domain;
		
	    domain = "";
	    return domain;
	}

	
	String getEmail(Document doc)
	{
		
		email = "";
		tempEmail="";
		try{

			Elements links = doc.select("a[href]");
			for(Element link : links)
			{
				tempEmail=link.attr("href");
				
				
				if(tempEmail.contains("mailto:"))
				{
					email=tempEmail.substring(7);
					break;
				}
			}
		}
		catch(Exception e)
		{
			//System.out.println("Apps Email not found");
			email="";
		}
		//System.out.println("developer :"+email);
		return email;
	}
	
	/*String getsiteGeoLocation(String siteLocation)
	{
		String siteGeoLocation = "";
		if (!siteLocation.isEmpty()) 
		{

			siteGeoLocation = GeoLocation.updateLocation(siteLocation);
			//System.out.println("geoLocation of website: "+siteGeoLocation);
		}
		return siteGeoLocation;
	}
	
	DBObject getGeoIP(String siteGeoLocation)
	{
		DBObject dbObject=null;
		

		if (siteGeoLocation.isEmpty()) {
			dbObject = (DBObject) JSON.parse(
					"{" + "'type' : 'MultiPoint'," + "'coordinates' : [ " + "[ 0.00, 0.00 ]" + "]" + "}" + "}");
		} else {
			try {
				//BasicDBObject doc1 = new BasicDBObject();
				String l[] = siteGeoLocation.split("/");

				String query1 = "'coordinates' : [ ";
				for (int i = 0; i < l.length; i++) {
					String lon[] = l[i].split(",");
					if (i == l.length - 1) {
						query1 += "[" + lon[1] + "," + lon[0] + "]";
						//System.out.println("LatLong:::" + lon[1] + " " + lon[0]);
					} else
						query1 += "[" + lon[1] + "," + lon[0] + "],";
				}
				query1 += "]";

				dbObject = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint'," + query1 + "" + "}}");
			} catch (Exception e) {
				dbObject = (DBObject) JSON.parse("{" + "'type' : 'MultiPoint'," + "'coordinates' : [ "
						+ "[ 0.00, 0.00 ]" + "]" + "}" + "}");

			}
		}
		
		
		return dbObject;
	}
	*/
	
	
public static void main(String[] args) throws IOException {
	
	/*new GetData().getWebsite(Jsoup.connect("https://play.google.com/store/apps/details?id=com.snapdeal.main")
							.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.120 Safari/535.2")
							.referrer("http://www.google.com") 
							.header("Accept-Language", "en")
							.timeout(10000)
							.followRedirects(true)
							.get());*/
	
	System.out.println("hello122 33 33".replaceAll("[^\\w\\s]",""));
	
}

}
